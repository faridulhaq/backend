package com.dynamix.java.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.dynamix.java.Application;
import com.dynamix.java.model.Employee;
import com.dynamix.java.repository.EmployeeRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc(addFilters = false)
public class EmployeeControllerTest {

    @Autowired
    MockMvc mvc;

    @MockBean
    EmployeeRepository repository;

    @Test
    public void getAllEmployeeTestApi() throws  Exception{
        Employee employee = new Employee("Omar","Ali","ali@gmail.com");
        employee.setEmployeeId(1);
        List<Employee> employees = Arrays.asList(employee);

        when(repository.findAll()).thenReturn(employees);

        MvcResult mvcResult = mvc.perform(get("/api/v1/employees").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        assertThat(mvcResult.getResponse().getContentAsString())
                .isEqualTo("[{\"employeeId\":1,\"firstName\":\"Omar\",\"lastName\":\"Ali\",\"emailId\":\"ali@gmail.com\",\"phone\":null}]");
    }

    @Test
    public void getEmployeeByIdTestApi() throws  Exception{
        Employee employee = new Employee(1L,"Omar", "Ali", "ali@gmail.com", null);

        when(repository.findById(1L)).thenReturn(Optional.of(employee));

        MvcResult mvcResult = mvc.perform(get("/api/v1/employees/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        assertThat(mvcResult.getResponse().getContentAsString())
                .isEqualTo("{\"employeeId\":1,\"firstName\":\"Omar\",\"lastName\":\"Ali\",\"emailId\":\"ali@gmail.com\",\"phone\":null}");
    }

    @Test
    public void createEmployeeTestAPI() throws Exception
    {
        Employee employee = new Employee(1L,"Omar", "Ali", "ali@gmail.com", null);
        when(repository.save(any(Employee.class))).thenReturn(employee);

        MvcResult mvcResult = mvc.perform(post("/api/v1/employees")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"employeeId\":1,\"firstName\":\"Omar\",\"lastName\":\"Ali\",\"emailId\":\"ali@gmail.com\",\"phone\":null}")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        assertThat(mvcResult.getResponse().getContentAsString())
                .isEqualTo("{\"employeeId\":1,\"firstName\":\"Omar\",\"lastName\":\"Ali\",\"emailId\":\"ali@gmail.com\",\"phone\":null}");
    }

    @Test
    public void updateEmployeeTestAPI() throws Exception
    {
        MvcResult mvcResult = mvc.perform(put("/api/v1/employees/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"employeeId\":1,\"firstName\":\"Omar\",\"lastName\":\"Ali\",\"emailId\":\"ali@gmail.com\",\"phone\":null}")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void testFooDeleteTestApi() throws Exception {
        MvcResult mvcResult = mvc.perform(delete("/api/v1/employees/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"employeeId\":1,\"firstName\":\"Omar\",\"lastName\":\"Ali\",\"emailId\":\"ali@gmail.com\",\"phone\":null}")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
    }


}