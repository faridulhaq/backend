package com.dynamix.java.util;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

public enum CnyType {
	CLIENT(Values.CLIENT),PROSPECT(Values.PROSPECT),END_CLIENT(Values.END_CLIENT);
	
	private CnyType(final String value){
		this.value=value;
	}
	
	private String value;
	
	public String getValue() {
		return value;
	}
	
	public static CnyType fromValue(String value) {
		switch(value) {
		case Values.CLIENT:
			return CnyType.CLIENT;
		case Values.PROSPECT:
			return CnyType.PROSPECT;
		case Values.END_CLIENT:
			return CnyType.END_CLIENT;
		default:
			throw new IllegalArgumentException("Value [" +value+" ] not supported.");
		}
	}
	
	@Override
	public String toString() {
		return this.value;
	}
	
	public static class Values{
		public static final String CLIENT = "Client";
		public static final String PROSPECT = "Prospect";
		public static final String END_CLIENT = "End Client";
	}
	
	@Converter(autoApply=true)
	public static class CnyTypeConverter implements AttributeConverter<CnyType,String>{

		@Override
		public String convertToDatabaseColumn(CnyType attribute) {
			return attribute == null ? null : attribute.getValue();
		}

		@Override
		public CnyType convertToEntityAttribute(String dbData) {
			return dbData == null ? null : CnyType.fromValue(dbData);
		}
	}
}
