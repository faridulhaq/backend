package com.dynamix.java.util;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.stream.Stream;

public enum Role {

    CEO(Values.CEO), SALES(Values.SALES), ACCOUNTANT(Values.ACCOUNTANT), ADVOCATE(Values.ADVOCATE);

    private String value;

    Role(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static class Values {
        public static final String CEO = "CEO";
        public static final String SALES = "Sales";
        public static final String ACCOUNTANT = "Accountant";
        public static final String ADVOCATE = "Advocate";
    }

    @Converter(autoApply = true)
    public static class AttibuteValueConverter implements AttributeConverter<Role, String> {

        @Override
        public String convertToDatabaseColumn(Role role) {
            if (role == null) {
                return null;
            }
            return role.getValue();
        }

        @Override
        public Role convertToEntityAttribute(String value) {
            if (value == null) {
                return null;
            }
            return Stream.of(Role.values())
                    .filter(c -> c.getValue().equals(value))
                    .findFirst()
                    .orElseThrow(IllegalArgumentException::new);
        }
    }
}