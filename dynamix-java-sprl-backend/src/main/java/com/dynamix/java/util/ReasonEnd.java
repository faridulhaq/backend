package com.dynamix.java.util;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.stream.Stream;


public enum ReasonEnd {
    
    MISSION_END(Values.MISSION_END), BUDGET_LACK(Values.BUDGET_LACK), CONSULTANT_PROBLEM(Values.CONSULTANT_PROBLEM), 
    OLD_TECHNOLOGY(Values.OLD_TECHNOLOGY);

    private String value;

    ReasonEnd(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public  String toString(){
        return this.value;
    }

    public static class Values {
        public static final String MISSION_END = "Mission End";
        public static final String BUDGET_LACK = "Budget Lack";
        public static final String CONSULTANT_PROBLEM = "Consultant Problem";
        public static final String OLD_TECHNOLOGY = "Old Technology";
    }

    @Converter(autoApply = true)
    public static class AttibuteValueConverter implements AttributeConverter<ReasonEnd, String> {

        @Override
        public String convertToDatabaseColumn(ReasonEnd reasonEnd) {
            if (reasonEnd == null) {
                return null;
            }
            return reasonEnd.getValue();
        }

        @Override
        public ReasonEnd convertToEntityAttribute(String value) {
            if (value == null) {
                return null;
            } 
            return Stream.of(ReasonEnd.values())
                    .filter(c -> c.getValue().equals(value))
                    .findFirst()
                    .orElseThrow(IllegalArgumentException::new);
        }
    }

    
}