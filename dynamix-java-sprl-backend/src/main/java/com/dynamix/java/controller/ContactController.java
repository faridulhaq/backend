package com.dynamix.java.controller;

import com.dynamix.java.exceptions.ResourceNotFoundException;
import com.dynamix.java.model.Contact;
import com.dynamix.java.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/contact")
public class ContactController {

    @Autowired
    private ContactService contactService;

    @GetMapping("/all")
    public List<Contact> getAllContacts() {
        return contactService.getAllContacts();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Contact> getContactById(@PathVariable(value = "id") Long id) throws ResourceNotFoundException {
        return ResponseEntity.ok().body(contactService.getContactById(id));
    }

    @PostMapping("/save")
    public Contact saveContact(@Valid @RequestBody Contact contact)  {
        return contactService.saveContact(contact);
    }

    @DeleteMapping("/{id}")
    public Map<String, Boolean> deleteContact(@PathVariable(value = "id") Long id)
            throws ResourceNotFoundException {
        contactService.deleteContact(id);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
