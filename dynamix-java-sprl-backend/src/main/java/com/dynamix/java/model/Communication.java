package com.dynamix.java.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "communication")
public class Communication {

    private Long id;
    private LocalDate communicationDate;
    private String content;

    private Contact contact;
    private Employee employee;

    public Communication() {
    }

    public Communication(LocalDate communicationDate, String content, Employee employee) {
        this.communicationDate = communicationDate;
        this.content = content;
        this.employee = employee;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="comm_id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @JsonFormat(pattern = "MM/dd/yyyy")
    @Column(name="comm_date", nullable = false)
    public LocalDate getCommunicationDate() {
        return communicationDate;
    }

    public void setCommunicationDate(LocalDate communicationDate) {
        this.communicationDate = communicationDate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "employee_id",referencedColumnName="employeeId", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "contact_id",referencedColumnName="contact_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    @Override
    public String toString() {
        return "Communication [communicationDate=" + communicationDate + ", content=" + content + ", employee="
                + employee + ", id=" + id + "]";
    }

}