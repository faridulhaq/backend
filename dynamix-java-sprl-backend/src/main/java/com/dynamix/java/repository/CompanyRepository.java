package com.dynamix.java.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dynamix.java.model.Company;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Long>{

}
