package com.dynamix.java.service;

import com.dynamix.java.exceptions.ResourceNotFoundException;
import com.dynamix.java.model.Resume;
import com.dynamix.java.model.Technology;
import com.dynamix.java.repository.ResumeRepository;
import com.dynamix.java.repository.TechnologyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class ResumeServiceImpl implements ResumeService {

    @Autowired
    private ResumeRepository resumeRepository;

    @Autowired
    private TechnologyRepository technologyRepository;

    @Override
    public Resume getResumeByConsultantId(Long id) throws ResourceNotFoundException {
        return resumeRepository.findByConsultant_ConsultantId(id).orElseThrow(() -> new ResourceNotFoundException("Resume not found for this Consultant Id :: " + id));
    }

    @Override
    public List<Resume> getAllResumes() {
        return resumeRepository.findAll();
    }

    @Override
    public List<Resume> getResumeByReceptionDate(LocalDate date) throws ResourceNotFoundException {
        return resumeRepository.findByReceptionDate(date).orElseThrow(() -> new ResourceNotFoundException("No Resume's found for this date::" + date));
    }

    @Override
    public Resume saveResume(Resume resume) {
        return resumeRepository.save(resume);
    }

    @Override
    public void deleteResume(Long id) throws ResourceNotFoundException {
        resumeRepository.findById(id).map(resume -> {
            resumeRepository.delete(resume);
            return null;
        }).orElseThrow(() -> new ResourceNotFoundException("Resume id " + id + " not found"));
    }

    @Override
    public Technology getTechnologyById(Long id) throws ResourceNotFoundException {
        return technologyRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Technology not found for this id :: " + id));
    }

    @Override
    public Technology saveTechnology(Technology technology) {
        return technologyRepository.save(technology);
    }

    @Override
    public void deleteTechnology(Long id) throws ResourceNotFoundException {
        Technology technology = getTechnologyById(id);
        technologyRepository.delete(technology);
    }


}
