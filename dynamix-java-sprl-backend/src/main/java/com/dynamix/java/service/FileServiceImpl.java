package com.dynamix.java.service;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dynamix.java.exceptions.ResourceNotFoundException;
import com.dynamix.java.model.File;
import com.dynamix.java.repository.FileRepository;


@Service
public class FileServiceImpl implements FileService {
	
	@Autowired
	private FileRepository fileRepository;

	@Override
	public List<File> getAllFiles() {
		return fileRepository.findAll();
	}

	@Override
	public List<File> getFilesByResumeId(@NotNull Long resumeId) {
		return fileRepository.findByResume_resumeId(resumeId);
	}

	@Override
	public File createFile(File file) {
		return fileRepository.save(file);
	}

	@Override
	public File updateFile(Long fileId, File fileDetails) throws ResourceNotFoundException {
		File file = fileRepository.findById(fileId)
				.orElseThrow(() -> new ResourceNotFoundException("File not found for this id :: " + fileId));
		
		file.setLocation(fileDetails.getLocation());
		file.setSize(fileDetails.getSize());
		file.setFileType(fileDetails.getFileType());
		file.setResume(fileDetails.getResume());
		
		final File updatedFile = fileRepository.save(file);
		return updatedFile;
	}

	@Override
	public void deleteFile(Long fileId) throws ResourceNotFoundException {
		File file = fileRepository.findById(fileId)
				    .orElseThrow(() -> new ResourceNotFoundException("File not found for this id :: " + fileId));
		fileRepository.delete(file);
	}
}
