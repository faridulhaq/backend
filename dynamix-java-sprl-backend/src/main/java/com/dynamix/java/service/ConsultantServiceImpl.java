package com.dynamix.java.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dynamix.java.exceptions.ResourceNotFoundException;
import com.dynamix.java.model.Consultant;
import com.dynamix.java.repository.ConsultantRepository;
import com.dynamix.java.repository.EmployeeRepository;

@Repository
public class ConsultantServiceImpl implements ConsultantService {

    @Autowired
    private ConsultantRepository consultantRepository;

    @Autowired
    private EmployeeRepository employeeRepository;
    

    @Override
    public List<Consultant> getAllConsultants() {
        return consultantRepository.findAll();
    }

    @Override
    public Consultant getConsultantById(Long id) throws ResourceNotFoundException {
        return consultantRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Consultant not found for this id :: " + id));
    }

    @Override
    public Consultant saveConsultant(Long employeeId, Consultant consultant) throws ResourceNotFoundException {
        return employeeRepository.findById(employeeId).map(employee -> {
            consultant.setEmployee(employee);
            return consultantRepository.save(consultant);
        }).orElseThrow(() -> new ResourceNotFoundException("EmployeeId " + employeeId + " not found"));
    }

    @Override
    public void deleteConsultant(Long id) throws ResourceNotFoundException {
        Consultant consultant = getConsultantById(id);
        consultantRepository.delete(consultant);
    }

	@Override
	public Consultant addConsultant(Consultant consultant) throws Exception{		
		return consultantRepository.save(consultant);
	}

	@Override
	public Consultant updateConsultant(Long consultantId, @Valid Consultant consultantDetails) throws ResourceNotFoundException {
		
		return consultantRepository.findById(consultantId).map(consultant -> {
			consultant.setAddress(consultantDetails.getAddress());
			consultant.setAvailabilityDate(consultantDetails.getAvailabilityDate());
			consultant.setEmail(consultantDetails.getEmail());
			consultant.setEmployee(consultantDetails.getEmployee());
			consultant.setFirstName(consultantDetails.getFirstName());
			consultant.setGender(consultantDetails.getGender());
			consultant.setLastName(consultantDetails.getLastName());
			consultant.setlinkedInUrl(consultantDetails.getlinkedInUrl());
			consultant.setMaritalStatus(consultantDetails.getMaritalStatus());
			consultant.setPhone(consultantDetails.getPhone());
			consultant.setPhoto(consultantDetails.getPhoto());
			consultant.setResume(consultantDetails.getResume());
			consultant.setTitle(consultantDetails.getTitle());
			return consultantRepository.save(consultant);
		}).orElseThrow(() -> new ResourceNotFoundException("Consultant not found for this id :: " + consultantId));
	}

}
