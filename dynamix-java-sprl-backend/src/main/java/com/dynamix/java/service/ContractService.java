package com.dynamix.java.service;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dynamix.java.exceptions.ResourceNotFoundException;
import com.dynamix.java.model.Contract;
import com.dynamix.java.repository.ContractRepository;

@Service
public class ContractService {

    @Autowired
    private ContractRepository contractRepository;
    
    public List<Contract> getAllContracts() {
		return contractRepository.findAll();
	}

	public Optional<Contract> getContractById(Long contractId) {
		return contractRepository.findById(contractId);
	}
	
	public Contract getContractByConsultantId(LocalDate endDate, Long consultantId) {
		return contractRepository.findByStartDateLessThanEqualAndEndDateGreaterThanEqualAndConsultant_consultantId(
				endDate, endDate, consultantId);
	}

	public Contract createContract(@Valid Contract contract) {
        return contractRepository.save(contract);
	}
	
	public Contract updateContract(@Valid Contract contractDetails, Long contractId) throws ResourceNotFoundException {
		Contract contract = getContractById(contractId)
				.orElseThrow(() -> new ResourceNotFoundException("Contract not found for this id : " + contractId));
		contract.setStartDate(contractDetails.getStartDate());
		contract.setEndDate(contractDetails.getEndDate());
		contract.setCompany(contractDetails.getCompany());
		contract.setEndClient(contractDetails.getEndClient());
		contract.setConsultant(contractDetails.getConsultant());
		return contractRepository.save(contract);
	}
	
	public Map<String, Boolean> deleteContract(Long contractId) throws ResourceNotFoundException {
        Contract Contract = contractRepository.findById(contractId)
				.orElseThrow(() -> new ResourceNotFoundException("Contract not found for this id :: " + contractId));
                contractRepository.delete(Contract);
        
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}
}