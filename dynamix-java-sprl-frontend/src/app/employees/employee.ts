

export class Employee {
    employeeId: number;
    firstName: string;
    lastName: string;
    phone: BigInteger;
    address: string;
    emailId: string;
    active: boolean;
}
