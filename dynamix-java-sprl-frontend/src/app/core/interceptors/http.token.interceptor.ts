import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';


@Injectable()
export class HttpTokenInterceptor implements HttpInterceptor {

  constructor(private authenticationService: AuthenticationService) { }


  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let headersConfig = {};
    if (req.url.includes('oauth/token')) {
      headersConfig = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Basic ' + btoa('dynadmin:dynaadmin-auth')
      };
    } else {
      headersConfig = {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      };
      const token = this.authenticationService.getToken();

      if (token) {
        headersConfig['Authorization'] = `Bearer ${token}`;
      }
    } 

    const request = req.clone({ setHeaders: headersConfig });
    return next.handle(request);
  }


}
