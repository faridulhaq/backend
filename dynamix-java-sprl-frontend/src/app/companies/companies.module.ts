import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CompaniesRoutingModule } from './companies-routing.module';
import { CompaniesComponent } from './companies.component';
import { CompanyViewComponent } from './company-view/company-view.component';
import { CompanyAddComponent } from './company-add/company-add.component';

import { AddressFormat } from '../address-format.pipe';

import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { DataViewModule } from 'primeng/dataview';
import { PanelModule } from 'primeng/panel';
import { DropdownModule } from 'primeng/dropdown';
import { FormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { SplitButtonModule } from 'primeng/splitbutton';
import { TabViewModule } from 'primeng/tabview';

@NgModule({
  declarations: [
    CompaniesComponent, 
    CompanyViewComponent,
    AddressFormat,
    CompanyAddComponent
  ],
  imports: [
    CommonModule,
    CompaniesRoutingModule,
    DataViewModule,
    PanelModule,
    DropdownModule,
    FormsModule,
    InputTextModule,
    SplitButtonModule,
    TabViewModule,
    TableModule,
    ButtonModule 
  ]
})
export class CompaniesModule { }
