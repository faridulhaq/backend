import { Injectable } from '@angular/core';
import {Observable, of} from "rxjs";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  constructor(private http: HttpClient) { }

  getCompanies(): Observable<any> {

    return this.http.get(`${environment.baseUrl}` + 'companies');

    // BELOW IS TEMPORARY, IT WILL BE REMOVED IN FUTURE
    
    /**
    * If you need to use an access_token directy in the request, then uncomment following:
    */

    /**
    return this.http.get(`${environment.baseUrl}` + 'companies', {
      params: {
        'access_token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1OTAwODMyMzYsInVzZXJfbmFtZSI6ImFkbWluIiwiYXV0aG9yaXRpZXMiOlsiTWFuYWdlciJdLCJqdGkiOiJjN2FhZTg0Zi1iNjJhLTRhN2UtOTAxYy1iNDlkNzI0ZjNmZTEiLCJjbGllbnRfaWQiOiJkeW5hZG1pbiIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSIsInRydXN0Il19._sUFryYJUNGJHf8G9kp5IAdpjfXEOxEh7l--kojUQck'
      }
    });
    */

    /**
    * If you need to use just dummy data then uncomment following:
    return this.http.get('assets/companies.json');
    */
  }

  getCompany( companyId: number ): Observable<any> {

    return this.http.get(`${environment.baseUrl}` + 'company' + `/${companyId}`);

    // BELOW IS TEMPORARY, IT WILL BE REMOVED IN FUTURE
    
    /**
    * If you need to use an access_token directy in the request, then uncomment following:
    */

    /**
    return this.http.get(`${environment.baseUrl}` + 'company' + `/${companyId}`, {
      params : {
        'access_token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1OTAwODMyMzYsInVzZXJfbmFtZSI6ImFkbWluIiwiYXV0aG9yaXRpZXMiOlsiTWFuYWdlciJdLCJqdGkiOiJjN2FhZTg0Zi1iNjJhLTRhN2UtOTAxYy1iNDlkNzI0ZjNmZTEiLCJjbGllbnRfaWQiOiJkeW5hZG1pbiIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSIsInRydXN0Il19._sUFryYJUNGJHf8G9kp5IAdpjfXEOxEh7l--kojUQck'
      } } ); 

    */

    /**
    * If you need to use just dummy data then uncomment following

    /**return of( 
      {
        "companyId": 1,
        "name": "CHOAM",
        "vatNumber": "111111111",
        "cnyType": "Client",
        "comments": "Company Comments",
        "active": true,
        "address": {
            "addressId": 1,
            "address1": "Space Guild",
            "address2": "Old Empire",
            "city": "Dune",
            "country": "Padish",
            "postalCode": 100100
        }
      } ); **/

  }
}
