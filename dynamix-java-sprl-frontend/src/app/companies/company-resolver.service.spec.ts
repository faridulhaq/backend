import { TestBed } from '@angular/core/testing';

import { CompanyResolverService } from './company-resolver.service';

describe('CompanyResolverService', () => {
  let service: CompanyResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CompanyResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
