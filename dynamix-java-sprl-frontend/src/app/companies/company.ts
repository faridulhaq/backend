import { Address } from "../address";

export class Company {
  companyId: number;
  name: string;
  vatNumber: string;
  cnyType: string;
  comments: string;
  address: Address;
  active: boolean;
}

export interface CompanyResolved {
  company: Company;
  error?: any;
}
