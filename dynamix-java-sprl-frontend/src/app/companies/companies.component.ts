import { Component, OnInit, ViewChild } from '@angular/core';
import { CompanyService } from './company.service';
import { Router } from '@angular/router';
import { SelectItem } from 'primeng/api/selectitem';
import { Company } from './company';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.css']
})
export class CompaniesComponent implements OnInit {

  companies: Company[];
  sortOptions: SelectItem[];
  sortKey: string;
  sortField: string;
  sortOrder: number;
  
  constructor( private companyService: CompanyService, private router: Router ) { }

  ngOnInit(): void {
    this.reloadData();

    this.sortOptions = [
      {label:'Name',value:'name'},
      {label:'Type',value:'cnyType'}
    ];
  }

  reloadData() {
    this.companyService.getCompanies().subscribe(companies => {
      this.companies = <Company[]> companies;
    });
  }

  selectCompany(event: Event, companyId: number) {
    this.router.navigate(['companies',companyId]);
    event.preventDefault();
  }

  onSortChange(event) {
    let value = event.value;

    if (value.indexOf('!') === 0) {
        this.sortOrder = -1;
        this.sortField = value.substring(1, value.length);
    }
    else {
        this.sortOrder = 1;
        this.sortField = value;
    }
  }

  handleAddCompanyClick() {
    //this.router.navigate(['companies', companyId]);
  }

}
