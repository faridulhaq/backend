import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { CompanyService } from './company.service';
import { CompanyResolved } from './company';

@Injectable({
  providedIn: 'root'
})
export class CompanyResolverService implements Resolve<CompanyResolved> {

  constructor( private companyService: CompanyService ) { }
  
  resolve(route: ActivatedRouteSnapshot, 
      state: RouterStateSnapshot): Observable<CompanyResolved> {
    
    const companyId = route.paramMap.get('companyId');

    
    if( isNaN(+companyId) ) {
      const message = `Company id was not a number: ${companyId}`;
      return of({company: null, error: message});
    }

    return this.companyService.getCompany(+companyId)
    .pipe( catchError( (error) => {
      return of( { company: null, error: error })}
    ));
  }
}
